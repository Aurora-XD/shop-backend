package com.twuc.shopbackend.controller;

import com.twuc.shopbackend.domain.Orders;
import com.twuc.shopbackend.domain.Products;
import com.twuc.shopbackend.repository.OrderRepository;
import com.twuc.shopbackend.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class OrderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private EntityManager em;

    @Test
    void should_return_initial_order() throws Exception {
        Products products = new Products("p1", 12D, "a", "sss");
        productRepository.save(products);
        em.flush();
        em.clear();
        Orders orders = new Orders(products, 2);
        orderRepository.save(orders);
        em.flush();
        em.clear();

        mockMvc.perform(get("/api/orders/1"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.num").value(2));

    }

    @Test
    void should_create_a_new_order() throws Exception {
        Products product = new Products("p1", 12D, "a", "sss");
        productRepository.save(product);
        em.flush();
        em.clear();

        //create a new order
        mockMvc.perform(post("/api/orders")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(product.getId().toString()))
                .andExpect(status().is(201))
                .andExpect(jsonPath("$.product.productName").value("p1"));

    }

    @Test
    void should_increase_the_num_when_add_same_product() throws Exception {
        Products product = new Products("p1", 12D, "a", "sss");
        productRepository.save(product);
        em.flush();
        em.clear();

        //create a new order
        mockMvc.perform(post("/api/orders")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(product.getId().toString()))
                .andExpect(status().is(201))
                .andExpect(jsonPath("$.product.productName").value("p1"));

        //add same product the num of order should be 2
        mockMvc.perform(post("/api/orders")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(product.getId().toString()))
                .andExpect(status().is(201))
                .andExpect(jsonPath("$.num").value(2));

    }

    @Test
    void should_delete_order() throws Exception {
        Products product = new Products("p1", 12D, "a", "sss");

        Products save1 = productRepository.save(product);

        Orders order = new Orders(save1, 1);

        Orders save = orderRepository.save(order);

        em.flush();
        em.clear();

        mockMvc.perform(delete("/api/orders")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(save.getId().toString()))
                .andExpect(status().is(200));

    }

    //    @Test
//    void should_create_a_order() throws Exception {
//        mockMvc.perform(post("/api/orders")
//                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
//                .content("1"))
//                .andExpect(status().is(201));
//    }


}
