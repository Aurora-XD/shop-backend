package com.twuc.shopbackend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.shopbackend.constract.RequestProduct;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_initial_product() throws Exception {
        mockMvc.perform(get("/api/products/1"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.productName").value("Cola1"));
    }

    @Test
    void should_create_product() throws Exception {
        RequestProduct requestProduct = new RequestProduct("Cola4", 1.5, "bottle",
                "http://lawson-philippines.com/wp-content/uploads/2016/01/LawsonCocaCola-800x531.jpg");

        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(new ObjectMapper().writeValueAsString(requestProduct)))
                .andExpect(status().is(201));

    }
}
