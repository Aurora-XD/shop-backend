CREATE TABLE IF NOT EXISTS products (
    id BIGINT AUTO_INCREMENT,
    prodName VARCHAR(64) NOT NULL,
    price double not null,
    prodUnit varchar(32) NOT null,
    imageURL varchar(256) NOT null,
    PRIMARY KEY (id)
);

