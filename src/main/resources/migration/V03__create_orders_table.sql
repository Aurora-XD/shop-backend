CREATE TABLE IF NOT EXISTS orders (
    id BIGINT AUTO_INCREMENT,
    prodId BIGINT,
    num int not NULL ,
    PRIMARY KEY (id),
    foreign key (prodId) references products(id)
);

