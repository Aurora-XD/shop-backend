package com.twuc.shopbackend.controller;

import com.twuc.shopbackend.domain.Orders;
import com.twuc.shopbackend.domain.Products;
import com.twuc.shopbackend.repository.OrderRepository;
import com.twuc.shopbackend.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/orders")
@CrossOrigin(origins = "*")
public class OrderController {

    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;

    public OrderController(OrderRepository orderRepository, ProductRepository productRepository) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
    }

    @GetMapping
    public ResponseEntity getAllOrders(){
        List<Orders> orders = orderRepository.findAll();
        return ResponseEntity.status(200)
                .body(orders);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Orders> getOrderByID(@PathVariable Long id){
        Orders order = orderRepository.findById(id).get();
        System.out.println(order.getNum());
        return ResponseEntity.status(200)
                .body(order);
    }

    @PostMapping
    public ResponseEntity createOrder(@RequestBody Long prodId){
        Products product = productRepository.findById(prodId).get();
        Optional<Orders> order = orderRepository.findByProduct(product);

        if(order.isPresent()){
            Orders save = order.get();
            save.setNum(save.getNum()+1);
            Orders saved = orderRepository.save(save);
            return ResponseEntity.status(201)
                    .body(saved);
        }else {
            Orders newOrder = new Orders(product,1);
            Orders saved = orderRepository.save(newOrder);
            return ResponseEntity.status(201)
                    .body(saved);
        }
    }

    @DeleteMapping
    public ResponseEntity deleteOrderById(@RequestBody Long id){
        orderRepository.deleteById(id);

        return ResponseEntity.status(200)
                .build();
    }


}
