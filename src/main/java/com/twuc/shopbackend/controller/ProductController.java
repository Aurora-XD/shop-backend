package com.twuc.shopbackend.controller;

import com.twuc.shopbackend.constract.RequestProduct;
import com.twuc.shopbackend.domain.Products;
import com.twuc.shopbackend.repository.ProductRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/products")
@CrossOrigin(origins = "*")
public class ProductController {

    private final ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @GetMapping
    public ResponseEntity getAllProducts(){
        List<Products> products = productRepository.findAll();
        return ResponseEntity.status(200)
                .body(products);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Products> getProduct(@PathVariable Long id){

        Products product = productRepository.findById(id).get();

//        System.out.println("=======================");
//        System.out.println(product.getProductName());

        return ResponseEntity.status(200)
                .body(product);
    }


    @PostMapping
    public ResponseEntity createProduct(@RequestBody RequestProduct requestProduct){
        Products product = new Products(requestProduct.getProdName(),
                                        requestProduct.getPrice(),
                                        requestProduct.getProdUnit(),
                                        requestProduct.getImageURL());

        Products saved = productRepository.save(product);

        return ResponseEntity.status(201)
                .body(saved);
    }

}
