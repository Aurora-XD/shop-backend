package com.twuc.shopbackend.repository;

import com.twuc.shopbackend.domain.Orders;
import com.twuc.shopbackend.domain.Products;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Orders,Long> {
    Optional<Orders> findByProduct(Products product);
}
