package com.twuc.shopbackend.constract;

public class RequestProduct {

    private String prodName;
    private Double price;
    private String prodUnit;
    private String imageURL;

    public RequestProduct() {
    }

    public RequestProduct(String prodName, Double price, String prodUnit, String imageURL) {
        this.prodName = prodName;
        this.price = price;
        this.prodUnit = prodUnit;
        this.imageURL = imageURL;
    }

    public String getProdName() {
        return prodName;
    }

    public Double getPrice() {
        return price;
    }

    public String getProdUnit() {
        return prodUnit;
    }

    public String getImageURL() {
        return imageURL;
    }
}
