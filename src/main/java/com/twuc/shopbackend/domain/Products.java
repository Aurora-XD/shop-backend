package com.twuc.shopbackend.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "products")
public class Products {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String prodName;
    private Double price;
    private String prodUnit;
    private String imageURL;

    public Products() {
    }

    public Products(String prodName, Double price, String prodUnit, String imageURL) {
        this.prodName = prodName;
        this.price = price;
        this.prodUnit = prodUnit;
        this.imageURL = imageURL;
    }

    public Long getId() {
        return id;
    }

    public String getProductName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return prodUnit;
    }

    public String getImageURL() {
        return imageURL;
    }


}
