package com.twuc.shopbackend.domain;

import javax.persistence.*;

@Entity
@Table(name = "orders")
public class Orders {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne
    @JoinColumn(name = "prodId")
    private Products product;
    private int num;

    public Orders() {
    }

    public Orders(Products product, int num) {
        this.product = product;
        this.num = num;
    }

    public Long getId() {
        return id;
    }

    public Products getProduct() {
        return product;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
