1. 建表
CREATE TABLE IF NOT EXISTS products (
    id BIGINT AUTO_INCREMENT,
    productName VARCHAR(64) NOT NULL,
    price double not null,
    unit varchar(32) NOT null,
    imageURL varchar(256) NOT null
    PRIMARY KEY (id)
);


2. 插入原始数据
INSERT INTO products (productName, price, unit, imageURL) VALUES
('Cola1',2,'bottle','https://www.savesafe.com.tw/ProdImg/1000/964/00/1000964_00_main.jpg?t=1930/1/1')
('Cola2',1,'bottle','https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/15-09-26-RalfR-WLC-0098.jpg/220px-15-09-26-RalfR-WLC-0098.jpg')
('Cola3',5,'bottle','https://image.cache.storm.mg/styles/smg-800x533-fp/s3/media/image/2016/07/22/20160722-121637_U7195_M177880_8ac2.jpg?itok=gHPOQQoI')

